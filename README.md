# IbanFirst

Le projet a été réalisé avec React JS, qui m'a semblé le choix le plus judicieux pour plusieurs raisons : les éléments se prêtent bien à la structure components, les données peuvent être mises à jour de façon continue, etc.

Je n'ai pas jugé nécessaire d'intégrer un store (like Redux) pour la simplicité du code mais aussi pour montrer que je maitrise la manipulation des state/props (voir *highlightCountry*).

Le solde consolidé est calculé grâce à la fonction récursive *updateTotal* qui fait appel à l'API IbanFirst (voir *getRate*). Ce solde est également calculé sur la base de la *selectedCurrency* (par défaut en EUR) que l'on peut modifier en USD par un simple clic (une fonction bonus appelée *changeCurrency*).

Pour la partie graphique, j'ai choisi d'utiliser Material-UI qui propose des components React, basés sur Material Design de Google. Cela permet de créer rapidement une interface avec des éléments de base (tables, toolbar, etc) pour ce type d'application (type middle-office) qui ne demande pas plus.

En ce qui concerne la cartographie, j'ai utilisé la librairie DataMaps qui offre une certaine liberté et qui n'utilise pas jQuery. La répartition des richesses par pays se fait sur la même base monétaire (voir *amountRef*) pour avoir une échelle de couleur juste.

## Installation

```
git clone git@gitlab.com:Kalizeau/ibanfirst.git
yarn install
yarn start
```

## Sources


*  IbanFirst : https://api.ibanfirst.com/APIDocumentation/PublicAPI
*  React JS : https://reactjs.org
*  Material-UI : https://material-ui.com
*  DataMaps : http://datamaps.github.io