export const counterpart = [{ 'CountryCode3': 'USA', 'Amout': '10 000.00', 'Currency': 'USD', 'Code': '1234564789', 'Bank': 'CHASUS33XXX', 'Owner': 'counterpart 1 compte USA' }, { 'CountryCode3': 'BEL', 'Amout': '500.00', 'Currency': 'EUR', 'Code': 'BE67914000046387', 'Bank': 'FXBBBEBBXXX', 'Owner': 'counterpart 2 compte BEL' }, { 'CountryCode3': 'FRA', 'Amout': '7 500.00', 'Currency': 'EUR', 'Code': '1234564789', 'Bank': 'FXFRBEBBXXX', 'Owner': 'counterpart 3 compte FRA' }]

export const getCounterpart = () => {
  return window.fetch('https://platform.ibanfirst.com/js/dataTestDevFront.json')
    .then(res => {
      if (!res.ok) throw new Error()
      return res.json()
    })
    .then(res => res.counterpart)
}

export const getRate = (from, to) => {
  return window.fetch(`https://api.ibanfirst.com/PublicAPI/Rate/${from}${to}/`)
    .then(res => res.json())
    .then(res => {
      if (res.status === '400') throw new Error(res.errorMessage)
      return res.rate.rate
    })
    .catch(err => console.log(err))
}
