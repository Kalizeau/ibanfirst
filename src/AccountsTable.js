import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

class AccountsTable extends React.Component {
  render () {
    return (
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Titulaire</TableCell>
              <TableCell align='right'>Code</TableCell>
              <TableCell align='right'>BIC</TableCell>
              <TableCell align='right'>Solde / Devise</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              this.props.data.accounts.map((account, index) => {
                return (
                  <TableRow key={index} style={this.props.data.selectedCountry === account.country ? { background: '#eeeeee' } : {}}>
                    <TableCell component='th' scope='row'>{account.owner}</TableCell>
                    <TableCell align='right'>{account.code}</TableCell>
                    <TableCell align='right'>{account.bank}</TableCell>
                    <TableCell align='right'>{account.amount.toFixed(2)} {account.currency}</TableCell>
                  </TableRow>
                )
              })
            }
          </TableBody>
        </Table>
      </Paper>
    )
  }
}

export default AccountsTable
