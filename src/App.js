import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import AccountsTable from './AccountsTable'
import ChoroplethMap from './ChoroplethMap'
import { getRate, counterpart } from './ApiService'
import './App.css'

class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      total: 0,
      accounts: [],
      selectedCurrency: 'EUR',
      selectedCountry: ''
    }
    this.updateTotal = this.updateTotal.bind(this)
    this.changeCurrency = this.changeCurrency.bind(this)
  }
  componentWillMount () {
    let accounts = []
    counterpart.forEach(account => {
      const currency = account.Currency
      const amount = Number(account.Amout.replace(' ', ''))
      let amountRef = amount
      if (currency !== 'EUR') {
        getRate(currency, 'EUR')
          .then(rate => {
            amountRef = amount * rate
          })
          .catch(error => console.log(error))
      }
      accounts.push({
        'country': account.CountryCode3,
        'amount': amount,
        'amountRef': amountRef,
        'currency': currency,
        'code': account.Code,
        'bank': account.Bank,
        'owner': account.Owner
      })
    })
    this.setState({ accounts })
  }
  componentDidMount () {
    this.updateTotal()
      .then(total => {
        this.setState({ total: total.toFixed(2) })
      })
      .catch(error => console.log(error))
  }
  updateTotal (total = 0, index = 0) {
    return new Promise((resolve, reject) => {
      if (this.state.accounts.length <= index) {
        resolve(total)
      }
      let account = this.state.accounts[index]
      if (account.currency !== this.state.selectedCurrency) {
        getRate(account.currency, this.state.selectedCurrency)
          .then(rate => {
            this.updateTotal(total + account.amount * rate, ++index)
              .then(total => {
                resolve(total)
              })
          })
          .catch(err => {
            reject(err)
          })
      } else {
        this.updateTotal(total + account.amount, ++index)
          .then(total => {
            resolve(total)
          })
      }
    })
  }
  changeCurrency () {
    this.setState({
      selectedCurrency: this.state.selectedCurrency === 'EUR' ? 'USD' : 'EUR'
    }, () => {
      this.updateTotal()
        .then(total => {
          this.setState({ total: total.toFixed(2) })
        })
        .catch(error => console.log(error))
    })
  }
  render () {
    return (
      <div>
        <div className='toolbar'>
          <AppBar position='static'>
            <Toolbar>
              <Typography style={{ flexGrow: 1 }} variant='title' color='inherit'>Liste des comptes</Typography>
              <Typography style={{ marginRight: 20 }} variant='subheading' color='inherit'>Solde consolidé</Typography>
              <Button onClick={() => this.changeCurrency()} variant='outlined' color='inherit'>{this.state.total} {this.state.selectedCurrency}</Button>
            </Toolbar>
          </AppBar>
        </div>
        <div className='container'>
          <div className='table'>
            <AccountsTable data={this.state} />
          </div>
          <div className='map'>
            <ChoroplethMap data={this.state} highlightCountry={selectedCountry => this.setState({ selectedCountry })} />
          </div>
        </div>
      </div>
    )
  }
}

export default App
