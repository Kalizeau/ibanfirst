import React from 'react'
import Datamap from 'datamaps/dist/datamaps.world.min.js'
import d3 from 'd3'

class ChoroplethMap extends React.Component {
  componentDidMount () {
    let onlyValues = this.props.data.accounts.map(account => { return account.amountRef })

    let minValue = Math.min.apply(null, onlyValues)
    let maxValue = Math.max.apply(null, onlyValues)

    let paletteScale = d3.scale.linear()
      .domain([minValue, maxValue])
      .range(['#9498ce', '#3f51b5'])

    let dataset = []

    this.props.data.accounts.forEach(account => {
      dataset[account.country] = {
        fillColor: paletteScale(account.amountRef)
      }
    })

    let highlightCountry = selectedCountry => {
      this.props.highlightCountry(selectedCountry)
    }

    let map = new Datamap({
      element: document.getElementById('cloropleth_map'),
      projection: 'mercator',
      responsive: true,
      aspectRatio: 0.67,
      geographyConfig: {
        popupOnHover: false,
        highlightFillColor: '#eeeeee',
        highlightBorderColor: '#aaaaaa',
        highlightBorderWidth: 0.5,
        borderColor: '#aaaaaa',
        borderWidth: 0.5
      },
      fills: {
        defaultFill: '#ffffff'
      },
      data: dataset,
      done: datamap => {
        datamap.svg.selectAll('.datamaps-subunit').on('click', geography => {
          highlightCountry(geography.id)
        })
      }
    })

    window.addEventListener('resize', function () {
      map.resize()
    })
  }
  render () {
    return (
      <div id='cloropleth_map' />
    )
  }
}

export default ChoroplethMap
